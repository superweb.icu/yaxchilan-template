# Yaxchilán - Web Template

HTML template for very graphic websites, based on [Bootstrap](https://getbootstrap.com/) 5. 

## About

The Yaxchilán template is open source. The HTML, CSS and JavaScript files are released under the [MIT license](LICENSE).

Yaxchilán was created by [maio](https://maio.digital/)

- https://maio.digital/
- https://twitter.com/maio_digital/

![Yaxchilán web template](yaxchilan_screenshot.jpg "Yaxchilán web template")

## Download

- [zip](https://gitlab.com/maio.webtemplates/yaxchilan-template/-/archive/master/yaxchilan-template-master.zip)
- [tar](https://gitlab.com/maio.webtemplates/yaxchilan-template/-/archive/master/yaxchilan-template-master.tar)
- Clone it with Git: <code>https://gitlab.com/maio.webtemplates/yaxchilan-template.git</code>

## License

HTML, CSS and JS code released under the MIT license.

Images are owned and licensed by [anunay rai](https://unsplash.com/@spaceforcopy?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) that requires you to publish attribution when using their images.